import fsPromises from 'node:fs/promises';
import replace from '@rollup/plugin-replace';
import terser from '@rollup/plugin-terser';

const {
	name,  version, description, keywords, engines, dependencies,
	author, license, homepage, repository, bugs,
} = JSON.parse(await fsPromises.readFile('./package.json', 'utf-8'));


const beginYear = 2022;
const year = new Date().getFullYear();

const banner = `\
/*!
 * ${ name } v${ version }
 * (c) ${ beginYear === year ? beginYear : `${ beginYear }-${ year }` } ${ author }
 * @license ${ license }
 */
`;

const outputFile = 'TaggedSql';
await fsPromises.writeFile('dist/package.json', JSON.stringify({
	name, version,
	main: `${ outputFile }.cjs`,
	types: `${ outputFile }.d.ts`,
	module: `${ outputFile }.mjs`,
	browser: `${ outputFile }.min.js`,
	unpkg: `${ outputFile }.js`,
	jsdelivr: `${ outputFile }.js`,
	dependencies, engines,
	description, keywords, author, license, homepage, repository, bugs,
	exports: {
		'.': {
			node: `./${ outputFile }.cjs`,
			types: `./${ outputFile }.d.ts`,
			module: `./${ outputFile }.mjs`,
			browser: `./${ outputFile }.min.js`,
			unpkg: `./${ outputFile }.js`,
			jsdelivr: `./${ outputFile }.js`,
		},
		'./package.json': './package.json',
	},
}, null, 2));
const input = 'src/index.mjs';
const output = `dist/${ outputFile }`;

const types = await fsPromises.readFile('src/types.mts', 'utf-8');


await fsPromises.writeFile(
	`dist/${ outputFile }.d.ts`,
	banner + types.replaceAll('__VERSION__', version)
);
await fsPromises.copyFile('README.md', 'dist/README.md');
await fsPromises.copyFile('LICENSE', 'dist/LICENSE');
export default {
	input,
	output: [
		{ format: 'cjs', file: `${ output }.cjs` },
		{ format: 'esm', file: `${ output }.mjs` },
		{ format: 'esm', file: `${ output }.min.mjs`, plugins: [terser()] },
		{ format: 'umd', file: `${ output }.js` },
		{ format: 'umd', file: `${ output }.min.js`,  plugins: [terser()] },
	].map(v => ({banner, exports: 'default', name: 'TaggedSql', ...v})),
	plugins: [
		replace({
			preventAssignment: true,
			values:{__VERSION__: version,
			},
		}),
	],
};
