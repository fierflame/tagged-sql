import Sql from './src/types.mjs';
declare global {
	export type TaggedSql = Sql;
	export namespace TaggedSql {
		export type Like = Sql.Like;
		export type Item = Sql.Item;
		export type Constructor = Sql.Constructor;
		export type FieldConstructor = Sql.FieldConstructor;
		export type Field = Sql.Field;
		export type TableConstructor = Sql.TableConstructor;
		export type Table = Sql.Table;
		export type IdConstructor = Sql.IdConstructor;
		export type Id = Sql.Id;

		export type ExprConstructor = Sql.ExprConstructor;
		export type Expr = Sql.Expr;
		export type FnExprConstructor = Sql.FnExprConstructor;
		export type FnExpr<T extends unknown[]> = Sql.FnExpr<T>;


		export type FnConstructor = Sql.FnConstructor;
		export type Fn<T extends unknown[]> = Sql.Fn<T>;


		export type GroupConstructor = Sql.GroupConstructor;
		export type Group = Sql.Group;

		export type Transformer = Sql.Transformer;
		export type SeparatorFn = Sql.SeparatorFn;
		export type Separator = Sql.Separator;
	}
}
