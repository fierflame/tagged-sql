#!/bin/env node
import assert from 'node:assert';
import test from 'node:test';
import Sql from '../src/index.mjs';
test('参数形式创建 sql 对象', async t => {
	const sql = Sql('SElECT ', '"name"', 'FROM', '"table"');
	await t.test('模板', () => assert.deepStrictEqual(
		sql.___template,
		['SElECT "name" FROM "table"']
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.___values,
		[]
	));
});
test('用标签模板创建 sql 对象', async t => {
	const sql = Sql`  SELECT  * FROM "table" WHERE "a" = ${ 1 } AND "b" > 2 OR "c" < ${ '2' }`;
	await t.test('模板', () => assert.deepStrictEqual(
		sql.___template,
		['SELECT  * FROM "table" WHERE "a" =', 'AND "b" > 2 OR "c" <', '']
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.___values,
		[1, '2']
	));
});


test('参数形式连接 sql', async t => {

	const table = new Sql.Table(Sql.Id('tableBooks'), 'tableAlias', false);
	const gTable = Sql.Table('tableUsers', true);

	const nf1 = Sql.Field('mainName');
	const nf2 = Sql.Field('mainAuthor');
	const f1 = Sql.Field(new Sql.Id('a'), table);
	const f2 = Sql.Field('c', Sql.Id('tableGroups'));
	const gf1 = Sql.Field('a', Sql.Table('tableRoles'), true);
	const gf2 = gTable.field('c');


	const sql = Sql('SELECT ', 1, ',', nf1, ',', nf2, ',', f1, ',', f2, ',', gf1, ',', gf2, Sql` FROM `, table, 'WHERE ', Sql`"a" = ${ 1 } AND "b" > 2`, 'OR ', Sql`"c" < ${ '2' }`, ' AND "d" > 3');
	await t.test('模板', () => assert.deepStrictEqual(
		sql.___template,
		['SELECT 1 ,', ',', ',', ',', ',', ',', 'FROM', 'WHERE "a" =', 'AND "b" > 2 OR "c" <', 'AND "d" > 3']
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.___values,
		[nf1, nf2, f1, f2, gf1, gf2, table, 1, '2']
	));
});

test('用标签模板连接 sql', async t => {

	const table = Sql.Table(Sql.Id('tableBooks'), Sql.Id('tableAlias'));
	const gTable = Sql.Table('tableUsers', true);

	const nf1 = Sql.Field('mainName');
	const nf2 = Sql.Field('mainAuthor');
	const f1 = Sql.Field(Sql.Id('a'), table);
	const f2 = Sql.Field('c', Sql.Id('tableGroups'));
	const gf1 = Sql.Field('a', Sql.Id('tableRoles'), true);
	const gf2 = gTable.field('c');
	const af1 = Sql.Field('d', Sql.Id('tableAlias', 'alias'));
	const sql = Sql`SELECT${ nf1 }, ${ undefined } ${ nf2 }, ${ f1 }, ${ f2 }, ${ gf1 },${ gf2 } ${ Sql`FROM ${ table }  ` } JOIN ${ gTable } WHERE ${ Sql`"a" = ${ 1 } AND "b" > 2` } OR ${ Sql`"c" < ${ '2' }` }`;
	await t.test('模板', () => assert.deepStrictEqual(
		sql.___template,
		['SELECT', ',', ',', ',', ',', ',', 'FROM', 'JOIN', 'WHERE "a" =', 'AND "b" > 2 OR "c" <', '']
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.___values,
		[nf1, nf2, f1, f2, gf1, gf2, table, gTable, 1, '2']
	));
});

test('sql 转字符串', async t => {

	const table = Sql.Table(Sql.Id('tableBooks'));
	const gTable = Sql.Table('tableUsers', true);
	const aTable = table.as('tableAlias');

	const nf1 = Sql.Id('mainName');
	const nf2 = Sql.Field('mainAuthor');
	const f1 = Sql.Field(Sql.Id('a'), table);
	const f2 = Sql.Field('c', Sql.Id('tableGroups'));
	const gf1 = Sql.Field('a', 'tableRoles', true);
	const gf2 = gTable.field('c');
	const af1 = Sql.Field('d', Sql.Id('tableAlias', 'alias'));
	const sql = Sql`SELECT${ nf1 }, ${ nf2 }, ${ f1 }, ${ f2 }, ${ gf1 },${ gf2 },${ af1 } ${ Sql`FROM ${ table }   ` } JOIN ${ gTable } JOIN ${ aTable } WHERE ${ Sql`"a" = ${ 1 } AND "b" > 2` } OR ${ Sql`"c" < ${ '2' }` }AND "d" > 3`;
	await t.test('模板', () => assert.deepStrictEqual(
		[
			sql.build('?'),
			sql.build((_, i) => `$${ i + 1 }`),
			sql.transform(v => v.replace(/([A-Z])/g, '_$1').toLowerCase()).build('?'),
			sql.transform(v => v.replace(/([A-Z])/g, '_$1').toLowerCase()).build((_, i) => `$${ i + 1 }`, '`'),
		],
		[
			'SELECT "mainName" , "mainAuthor" , "tableBooks"."a" , "tableGroups"."c" , "tableRoles"."a" , "tableUsers"."c" , "tableAlias"."d" FROM "tableBooks" JOIN "tableUsers" JOIN "tableBooks" AS "tableAlias" WHERE "a" = ? AND "b" > 2 OR "c" < ? AND "d" > 3',
			'SELECT "mainName" , "mainAuthor" , "tableBooks"."a" , "tableGroups"."c" , "tableRoles"."a" , "tableUsers"."c" , "tableAlias"."d" FROM "tableBooks" JOIN "tableUsers" JOIN "tableBooks" AS "tableAlias" WHERE "a" = $1 AND "b" > 2 OR "c" < $2 AND "d" > 3',
			'SELECT "main_name" , "main_author" , "table_books"."a" , "table_groups"."c" , "table_roles"."a" , "table_users"."c" , "table_alias"."d" FROM "table_books" JOIN "table_users" JOIN "table_books" AS "table_alias" WHERE "a" = ? AND "b" > 2 OR "c" < ? AND "d" > 3',
			'SELECT `main_name` , `main_author` , `table_books`.`a` , `table_groups`.`c` , `table_roles`.`a` , `table_users`.`c` , `table_alias`.`d` FROM `table_books` JOIN `table_users` JOIN `table_books` AS `table_alias` WHERE "a" = $1 AND "b" > 2 OR "c" < $2 AND "d" > 3',
		],
	));
	await t.test('模板(默认)，带表格前缀', () => assert.deepStrictEqual(
		[
			sql.transform((v, t, g) => g ? v : t === 'table' ? `tp_${ v }` : t === 'alias' ? `ta_${ v }` : v).build('?'),
			sql.transform((v, t, g) => g ? v : t === 'table' ? `tp_${ v }` : t === 'alias' ? `ta_${ v }` : v).build((_, i) => `$${ i + 1 }`),
			sql.transform((v, t, g) => (g ? v : t === 'table' ? `tp_${ v }` : t === 'alias' ? `ta_${ v }` : v).replace(/([A-Z])/g, '_$1').toLowerCase()).build('?'),
			sql.transform((v, t, g) => (g ? v : t === 'table' ? `tp_${ v }` : t === 'alias' ? `ta_${ v }` : v).replace(/([A-Z])/g, '_$1').toLowerCase()).build((_, i) => `$${ i + 1 }`),
		],
		[
			'SELECT "mainName" , "mainAuthor" , "tp_tableBooks"."a" , "tp_tableGroups"."c" , "tableRoles"."a" , "tableUsers"."c" , "ta_tableAlias"."d" FROM "tp_tableBooks" JOIN "tableUsers" JOIN "tp_tableBooks" AS "ta_tableAlias" WHERE "a" = ? AND "b" > 2 OR "c" < ? AND "d" > 3',
			'SELECT "mainName" , "mainAuthor" , "tp_tableBooks"."a" , "tp_tableGroups"."c" , "tableRoles"."a" , "tableUsers"."c" , "ta_tableAlias"."d" FROM "tp_tableBooks" JOIN "tableUsers" JOIN "tp_tableBooks" AS "ta_tableAlias" WHERE "a" = $1 AND "b" > 2 OR "c" < $2 AND "d" > 3',
			'SELECT "main_name" , "main_author" , "tp_table_books"."a" , "tp_table_groups"."c" , "table_roles"."a" , "table_users"."c" , "ta_table_alias"."d" FROM "tp_table_books" JOIN "table_users" JOIN "tp_table_books" AS "ta_table_alias" WHERE "a" = ? AND "b" > 2 OR "c" < ? AND "d" > 3',
			'SELECT "main_name" , "main_author" , "tp_table_books"."a" , "tp_table_groups"."c" , "table_roles"."a" , "table_users"."c" , "ta_table_alias"."d" FROM "tp_table_books" JOIN "table_users" JOIN "tp_table_books" AS "ta_table_alias" WHERE "a" = $1 AND "b" > 2 OR "c" < $2 AND "d" > 3',
		],
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.values,
		[1, '2']
	));
});

test('Table 的别名与字段方法', async t => {
	const t1 = Sql.Table('t1');
	const t2 = t1.as('t2');
	const f1 = t1.field('f1');
	const f2 = t2.field('f2');
	function transformer(v, t, g) {
		return `${ t }_${ v }`;
	}
	assert.deepStrictEqual([
		t1.transform(transformer).build(),
		t2.transform(transformer).build(),
		f1.transform(transformer).build(),
		f2.transform(transformer).build(),
	], [
		'"table_t1"',
		'"table_t1" AS "alias_t2"',
		'"table_t1"."field_f1"',
		'"alias_t2"."field_f2"',
	]);
});

/**
 * @type {TaggedSql.Fn<[any, any]>}
 */
const add = Sql.Fn('add', 2);

test('函数与表达式', async t => {
	const nf1 = Sql.Field('mainName');
	const sql = Sql`WHERE ${ Sql.Expr('+', [2, add(Sql.Expr('+', [5, nf1]), add(8, nf1))]) } + ${ Sql.Expr('+', [undefined, nf1]) }`
		.transform((v, t, g) => {
			if (t === 'operator') { return v + v; }
			if (t === 'fn') { return v.toUpperCase(); }
			return v;
		});
	await t.test('模板', () => assert.deepStrictEqual(
		sql.build('?'),
		'WHERE ? ++ ADD( ? ++ "mainName" , ADD( ? , "mainName" ) ) + ++ "mainName"',
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.values,
		[2, 5, 8]
	));
});

test('OR 运算符', async t => {
	const nf1 = Sql.Field('mainName');
	const sql = Sql`WHERE ${ Sql.Expr('OR', [Sql`${ nf1 } = ${ 1 }`, Sql.Expr('=', [nf1, 2]), Sql.Expr('=', [nf1, 3]), Sql.Expr('=', [nf1, 4])]) }`;
	await t.test('模板', () => assert.deepStrictEqual(
		sql.build('?'),
		'WHERE "mainName" = ? OR "mainName" = ? OR "mainName" = ? OR "mainName" = ? ',
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.values,
		[1, 2, 3, 4]
	));
});

test('IN 运算符与组', async t => {
	const nf1 = Sql.Field('mainName');
	const sql = Sql`WHERE ${ Sql.Expr('IN', [nf1, Sql.Group([1, 2, 3])]) }`;
	await t.test('模板', () => assert.deepStrictEqual(
		sql.build('?'),
		'WHERE "mainName" IN ( ? , ? , ? )',
	));
	await t.test('内容', () => assert.deepStrictEqual(
		sql.values,
		[1, 2, 3]
	));
});
