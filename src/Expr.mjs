import Sql from './Sql.mjs';
import addTemplate, { appendTemplate } from './addTemplate.mjs';
import defineProp from './defineProp.mjs';

/**
 * @this {TaggedSql.Expr?}
 * @param {string} operator
 * @param {*[]} operands
 * @returns {TaggedSql.Expr}
 */
function ExprConstructor(operator, operands) {
	/** @type {TaggedSql.Expr} */
	const that = this instanceof Expr ? this : Object.create(Expr.prototype);
	that.operator = operator;
	that.operands = operands;
	return that;
}

/** @type {TaggedSql.ExprConstructor} */
const Expr = /** @type {*} */(ExprConstructor);

Object.setPrototypeOf(Expr.prototype, Sql.prototype);
Object.setPrototypeOf(Expr, Sql);

defineProp(Expr.prototype, '__template', function(quote) {
	const { operands: [first, ...values], operator} = this;
	/** @type {string[]} */
	const template = [''];
	if (first !== undefined) {
		addTemplate(template, first, '', quote);
	}
	const others = values.filter(v => v !== undefined);
	if (others.length) {
		for (const other of others) {
			appendTemplate(template, operator);
			addTemplate(template, other, '', quote);
		}
	} else {
		appendTemplate(template, operator);
	}
	return template;
});

defineProp(Expr.prototype, '__values', function() {
	const { operands } = this;
	return operands.map(v => v instanceof Sql ? v.__values() : v === undefined ? [] : [v]).flat();
});

defineProp(Expr.prototype, 'transform', function(transformer) {
	const {operator, operands} = this;
	return Expr(
		transformer(operator, 'operator'),
		operands.map(v => v instanceof Sql ? v.transform(transformer) : v),
	);
});
export default Expr;
