import Sql from './Sql.mjs';
import isLike from './isLike.mjs';
import rmSpaces from './rmSpaces.mjs';

/**
 *
 * @param {readonly string[]} t
 * @param {any[]} value
 * @returns {[string[], any[]]}
 */
export default function parse(t, value) {
	const texts = t.slice(1);
	const first = rmSpaces(t[0]);
	/** @type {string[]} */
	const template = [];
	let end = first ? [first] : [];
	/** @type {any[]} */
	const values = [];
	/** @param {any} sql */
	function addSql(sql) {
		if (!(sql instanceof Sql)) {
			return false;
		}
		const {___template, ___values} = sql;
		if (!Array.isArray(___template) || !Array.isArray(___values)) {
			return false;
		}
		const [first, ...s] = ___template;
		if (first) { end.push(first); }
		if (s.length) {
			template.push(end.join(' '));
			const last = s.pop();
			end = last ? [last] : [];
			values.push(...___values);
			template.push(...s);
		}
		return true;
	}
	for (let i = 0; i < texts.length; i++) {
		const item = value[i];
		const it = rmSpaces(texts[i]);
		if (item === undefined) {
			if (it) { end.push(it); }
			continue;
		}
		if (addSql(item)) {
			if (it) { end.push(it); }
			continue;
		}
		if (item instanceof Sql || !isLike(item)) {
			template.push(end.join(' '));
			values.push(item);
			end = it ? [it] : [];
			continue;
		}
		const v = item.toTaggedSql();
		if (typeof v === 'string') {
			if (v) { end.push(v); }
			if (it) { end.push(it); }
		}
		if (addSql(v)) {
			if (it) { end.push(it); }
			continue;
		}
		template.push(end.join(' '));
		values.push(v);
		end = it ? [it] : [];
		continue;
	}
	template.push(end.join(' '));
	return [template, values];
}
