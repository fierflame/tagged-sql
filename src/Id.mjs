import Sql from './Sql.mjs';
import defineProp from './defineProp.mjs';
import getId from './getId.mjs';
/**
 * @this {TaggedSql.Id?}
 * @param {string} id
 * @param {string} [group]
 * @returns {TaggedSql.Id}
 */
function IdConstructor(id, group) {
	/** @type {TaggedSql.Id} */
	const that = this instanceof Id
		? this
		: Object.create(Id.prototype);
	that.id = id;
	that.group = group;
	return that;

}
/** @type {TaggedSql.IdConstructor} */
const Id = /** @type {*} */(IdConstructor);
Object.setPrototypeOf(Id.prototype, Sql.prototype);
Object.setPrototypeOf(Id, Sql);

defineProp(Id.prototype, '__values', () => []);
/**
 *
 * @param {TaggedSql.Id} param0
 * @param {'`' | '"'} [quote]
 * @returns {string}
 */
function toString({id}, quote) {
	return getId(id, quote);
}

defineProp(Id.prototype, 'build', function(_separator, quote) {
	return toString(this, quote);
});
defineProp(Id.prototype, '__template', function(quote) {
	return [toString(this, quote)];
});

defineProp(Id.prototype, 'transform', function(transformer) {
	const {id, group} = this;
	return new Id(transformer(id, group), group);
});
export default Id;
