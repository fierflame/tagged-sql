import FnExpr from './FnExpr.mjs';
import defineProp from './defineProp.mjs';

/**
 * @template {unknown[]} T
 * @param {string} name
 * @param {number} [length]
 * @returns {TaggedSql.Fn<T>}
 */
function FnConstructor(name, length = 0) {
	/** @type {TaggedSql.Fn<T>} */
	const that = function(...args) { return new FnExpr(name, args); };
	defineProp(that, 'name', name);
	defineProp(that, 'length', length);
	return that;
}

/** @type {TaggedSql.FnConstructor} */
const Fn = /** @type {*} */(FnConstructor);

export default Fn;
