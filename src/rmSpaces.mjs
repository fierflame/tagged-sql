/**
 *
 * @param {string} s
 * @returns {string}
 */
export default function rmSpaces(s) {
	return s.replace(/^\s+|\s+$/g, '');
}
