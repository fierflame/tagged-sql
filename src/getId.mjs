/**
 *
 * @param {string} id
 * @param {'`' | '"'} [quote]
 * @returns
 */
export default function getId(id, quote) {
	if (quote === '`') {
		return `\`${ id.replace(/`/g, '``') }\``;
	}
	return `"${ id.replace(/"/g, '""') }"`;
}
