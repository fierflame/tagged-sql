import Sql from './Sql.mjs';
import isLike from './isLike.mjs';
import rmSpaces from './rmSpaces.mjs';

/**
 *
 * @param {any[]} args
 * @returns {[string[], any[]]}
 */
export default function link(args) {
	/** @type {any[]} */
	const values = [];
	/** @type {string[]} */
	const template = [];
	/** @type {string[]} */
	let end = [];
	/** @param {*} item */
	function add(item) {
		if (item === undefined) { return true; }
		if (typeof item === 'string') {
			const v = rmSpaces(item);
			if (v) { end.push(v); }
			return true;
		}
		if (typeof item === 'number') {
			end.push(String(item));
			return true;
		}
		if (!(item instanceof Sql)) {
			return false;
		}
		const {___template, ___values} = item;
		if (!Array.isArray(___template) || !Array.isArray(___values)) {
			template.push(end.join(' '));
			values.push(item);
			end = [];
			return true;
		}
		const [f, ...t] = ___template;
		if (f) { end.push(f); }
		if (!t.length) { return true; }
		template.push(end.join(' '));
		values.push(...___values);
		const last = t.pop();
		template.push(...t);
		end = last ? [last] : [];
		return true;
	}
	for (const item of args) {
		if (add(item)) { continue; }
		if (isLike(item)) {
			add(item.toTaggedSql());
		}
	}
	template.push(end.join(' '));
	return [template, values];
}
